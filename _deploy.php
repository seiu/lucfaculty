<?php

date_default_timezone_set('America/New_York');
class Deploy {

  public $post_deploy;
  private $_log = '_deployments.log';
  private $_date_format = 'Y-m-d H:i:sP';
  private $_branch = 'master';
  private $_remote = 'origin';
  private $_directory = '';

  public function __construct($directory, $options = array())
  {
      $this->_directory = realpath($directory).DIRECTORY_SEPARATOR;
      $available_options = array('log', 'date_format', 'branch', 'remote');
      foreach ($options as $option => $value)
      {
          if (in_array($option, $available_options))
          {
              $this->{'_'.$option} = $value;
          }
      }
      $this->log('Attempting deployment...');
  }

  public function log($message, $type = 'INFO')
  {
      if ($this->_log)
      {
          $filename = $this->_log;
          if ( ! file_exists($filename))
          {
              file_put_contents($filename, '');
              chmod($filename, 0666);
          }
          file_put_contents($filename, date($this->_date_format).' --- '.$type.': '.$message.PHP_EOL, FILE_APPEND);
      }
  }

  public function execute()
  {
      try
      {
          exec('cd '.$this->_directory, $output);
          $this->log('Changing working directory... '.implode(' ', $output));
          exec('git reset --hard HEAD', $output);
          $this->log('Reseting repository... '.implode(' ', $output));
          exec('git pull '.$this->_remote.' '.$this->_branch, $output);
          $this->log('Pulling in changes... '.implode(' ', $output));
          exec('chmod -R og-rx .git');
          $this->log('Securing .git directory... ');
          if (is_callable($this->post_deploy))
          {
              call_user_func($this->post_deploy, $this->_data);
          }
          $this->log('Deployment successful.');
      }
      catch (Exception $e)
      {
          $this->log($e, 'ERROR');
      }
  }
}

$deploy = new Deploy($_directory);
$deploy->execute();
?>